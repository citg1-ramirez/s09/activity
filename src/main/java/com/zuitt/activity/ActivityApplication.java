package com.zuitt.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	ArrayList <String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enroll (@RequestParam(value="name", defaultValue = "name") String name){
		enrollees.add(name);
		return String.format("Thank you for enrolling, %s!", name);
	}

	@GetMapping("/getEnrollees")
	public String enrollees (){
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value = "name", defaultValue = "name") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s! My age is  %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id){
		String message = "";
		if(id.equalsIgnoreCase("java101"))
			message = "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000.00";
		else if(id.equalsIgnoreCase("sql101"))
			message = "Name: SQL 101, Schedule: TTH 1:00 PM - 4:00 PM, Price: PHP 2000.00";
		else if(id.equalsIgnoreCase("javaee101"))
			message = "Name: Java EE 101, Schedule: MWF 1:00 PM - 4:00 PM, Price: PHP 3500.00";
		else
			message = "Course cannot be found.";

		return message;
	}

	//Did only one return at the end of the method because I think its neater and was told it was proper coding practice :>
}
